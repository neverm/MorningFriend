# MorningFriend

### Description
This alarm clock Android application is aimed for people that are not
exactly satisfied with gentle and forgiving alarm clock applications.
It provides a set of puzzles that user has to solve when alarm goes off.
The alarm won't give up easily, so don't bother just quitting the app
or turning the volume down.

#### Puzzle types:

##### 1. Untagle Graph. 
Untangle graph requires you to move vertices (black dots) in such a way so
that no edges (lines) cross each other.

Difficulty increases amount of vertices and edges between them.

##### 2. Labyrinth.
In this puzzle you have to move right dot into right bottom corner
(where the exit is located).
Move the dot by pressing on the corresponding area of the screen:
right part to move right, top part to move top, etc.

Difficulty increases size of the labyrinth.

##### 3. Equation.
You have to solve given equation for x and insert the answer into the answer
input field. You have three lives and every incorrect answer reduces remaining
lives by 1. When you hit 0 you will get another equation.

Difficulty increases size of both parts of the equation.

##### 4. Squares
This is not really a puzzle but the implementation of default "snooze/disable"
functionality found in most alarm clock apps for those who for some reason need it.
Click on the red square to disble and on the green one to snooze.

Difficulty doesn't change anything.

